module "ecs_ireland" {
  source = "../module"
  instance_type = "t3.large" 
  max_size = 4
  min_size = 1
  domain_name = "devops_exercise.new10.io"
}

